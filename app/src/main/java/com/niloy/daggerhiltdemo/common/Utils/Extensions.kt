package com.niloy.daggerhiltdemo.common.Utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.textfield.TextInputLayout
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import timber.log.Timber
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException


// Any
inline fun runWithoutException(block: () -> Unit) {
    try {
        block()
    } catch (e: Exception) {
        Timber.e(e)
    }
}

/*
* [Context] Extension Function
*/
inline fun <reified T> Context.startActivity() {
    startActivity(Intent(this,T::class.java))
}

inline fun <reified T> Context.clearStackAndStartActivity() {
    startActivity(Intent(this,T::class.java).apply {
        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    })
}

fun Context.openDialerWithNumber(contactNo: String) {
    val intent = Intent(Intent.ACTION_DIAL).apply {
        data = Uri.parse("tel:$contactNo")
    }
    startActivity(intent)
}

fun Context.sendEmail(emails: Array<String>, subject: String?, body: String?) {
    val intent = Intent(Intent.ACTION_SENDTO).apply {
        data = Uri.parse("mailto:")
        putExtra(Intent.EXTRA_EMAIL, emails)
        subject?.let { putExtra(Intent.EXTRA_SUBJECT, subject) }
        body?.let { putExtra(Intent.EXTRA_TEXT, body) }
    }
    try {
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
    }
}

/*
* [Activity] Extension Function
*/
fun AppCompatActivity.makeToast(text: String?) {
    text?.let { Toast.makeText(this,text,Toast.LENGTH_SHORT).show() }
}

fun AppCompatActivity.setActionBarTitle(title: String) {
    this.supportActionBar?.title = title
}

/*
* [Exception]
*/

val Exception.networkErrorMessage: String
get() {
    Timber.e(this)
    return when(this) {
        is ConnectException -> "Unable to connect to the server. Please check your connection"
        is SocketTimeoutException -> "Connection timeout please try again"
        is IOException -> this.message ?: "IO error occurred"
        else -> "Error occurred please try again"
    }
}

/*
* [Fragment] Extension Function
*/
fun Fragment.makeToast(text: String?) {
    this.context?.let { context ->
        text?.let { Toast.makeText(context,text,Toast.LENGTH_SHORT).show() }
    }
}
// Moshi
inline fun <reified T> List<T>.fromListToJSON(): String {
    val type = Types.newParameterizedType(List::class.java, T::class.java)
    val adapter: JsonAdapter<List<T>> = Moshi.Builder().build().adapter(type)
    return adapter.toJson(this)
}

inline fun <reified T> String.fromJSONToList(): List<T> {
    val type = Types.newParameterizedType(List::class.java, T::class.java)
    val adapter: JsonAdapter<List<T>> = Moshi.Builder().build().adapter(type)
    return adapter.fromJson(this) ?: listOf()
}

inline fun <reified T> String.fromJSONToPOJO(): T {
    val adapter = Moshi.Builder().build().adapter(T::class.java)
    return adapter.fromJson(this)!!
}

inline fun <reified T> T.fromPOJOToJSON(): String {
    val adapter = Moshi.Builder().build().adapter(T::class.java)
    return adapter.toJson(this)
}

/*
* TextInput
* */
fun TextInputLayout.ifEmptySetError(msg: String = "Can't be empty"): Boolean {
    return if (this.editText?.text.toString().isEmpty()) {
        this.error = msg
        false
    } else {
        this.error = null
        true
    }
}
/*
* [ViewPager2] Extension Function
*/

val ViewPager2.recyclerView: RecyclerView
    get() {
        return this[0] as RecyclerView
    }

/*
* [MutableLiveData] Extension Function
*/

fun <T> MutableLiveData<T>.setValueThenNullify(value: T) {
    this.value = value
    this.value = null
}