package bd.edu.daffodilvarsity.classmanager.utils

import okio.IOException

class TokenExpirationException : IOException("Login session expired please sign out & login again")

class UnauthorizationException(errorMessage : String = "Unauthorized") : IOException(errorMessage)

class ServerErrorException(errorMessage : String = "Internal Server error please try again") : IOException(errorMessage)

class TimeOutException(errorMessage: String = "Request Timeout") : IOException(errorMessage)

class ForbiddenException(errorMessage: String = "Permission forbidden") : IOException(errorMessage)

class UnknownException(errorMessage: String = "Unknown error.Please try again") : IOException(errorMessage)

class CustomException(errorMessage: String) : IOException(errorMessage)
