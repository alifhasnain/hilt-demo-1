package com.niloy.daggerhiltdemo.common.adapter

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import coil.load
import coil.request.CachePolicy
import com.niloy.daggerhiltdemo.common.models.LoadStates

@BindingAdapter("bindImage")
fun bindImage(imageView: ImageView, url: String) {
    val placeHolder = CircularProgressDrawable(imageView.context).apply {
        strokeWidth = 10f
        centerRadius = 70f
        setColorSchemeColors(
            Color.parseColor("#F2831F"),
            Color.parseColor("#9D1E25"),
            Color.parseColor("#192023")
        )
    }
    imageView.load(url) {
        crossfade(true)
        placeholder(placeHolder)
        memoryCachePolicy(CachePolicy.DISABLED)
        diskCachePolicy(CachePolicy.DISABLED)
    }
    placeHolder.start()
}

@BindingAdapter("visibility")
fun visibility(view: View,visible: Boolean) {
    if(visible) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}

@BindingAdapter("bindLoadingState")
fun uiStateWhileLoading(view: View, state: LoadStates?) {
    state?.let {
        view.visibility = if (state is LoadStates.Loading) { View.VISIBLE } else { View.GONE }
    }
}

@BindingAdapter("bindErrorState")
fun uiStateWhenError(view: LinearLayout, state: LoadStates?) {
    state?.let {
        view.visibility = if(state is LoadStates.Error) {
            val msg = view.getChildAt(1) as TextView
            msg.text = state.msg
            View.VISIBLE
        } else { View.GONE }
    }
}

@BindingAdapter("bindEmptyState")
fun uiStateWhenEmpty(view: LinearLayout, state: LoadStates?) {
    state?.let {
        if(state is LoadStates.Empty) {
            val msg = view.getChildAt(1) as TextView
            msg.text = state.msg
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }
}

@BindingAdapter("bindNonEmptyState")
fun uiStateWhenNotEmpty(view: View, state: LoadStates?) {
    state?.let {
        view.visibility = if (state is LoadStates.NotEmpty) { View.VISIBLE } else { View.GONE }
    }
}

@BindingAdapter("bindDisableState")
fun bindButtonDisableState(view: View, state: LoadStates) {
    view.isEnabled = state !is LoadStates.Loading
}