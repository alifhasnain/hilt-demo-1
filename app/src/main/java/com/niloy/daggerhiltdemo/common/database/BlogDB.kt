package com.niloy.daggerhiltdemo.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.niloy.daggerhiltdemo.common.models.Blog

@Database(entities = [Blog::class], version = 1, exportSchema = false)
abstract class BlogDB: RoomDatabase() {

    abstract fun blogDAO(): BlogDao

    companion object {
        const val DB_NAME = "blog-db"
    }
}