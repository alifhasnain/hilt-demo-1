package com.niloy.daggerhiltdemo.common.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.niloy.daggerhiltdemo.common.models.Blog

@Dao
interface BlogDao {

    @Insert
    suspend fun insertBlogs(blog: List<Blog>)

    @Query("delete from blogs")
    suspend fun clearBlogs()

    @Transaction
    suspend fun cacheBlogs(blogs: List<Blog>) {
        clearBlogs()
        insertBlogs(blogs)
    }

    @Query("select * from blogs")
    fun getCachedBlogsLiveData(): LiveData<List<Blog>>

}