package com.niloy.daggerhiltdemo.common.di

import com.niloy.daggerhiltdemo.common.database.BlogDao
import com.niloy.daggerhiltdemo.common.network.BlogAPI
import com.niloy.daggerhiltdemo.common.repos.BlogsRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object RepositoryModule {

    @Singleton
    @Provides
    fun provideRepository(
        blogDao: BlogDao,
        blogService: BlogAPI
    ): BlogsRepo = BlogsRepo(blogDao, blogService)

}