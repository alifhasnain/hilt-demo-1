package com.niloy.daggerhiltdemo.common.di

import bd.edu.daffodilvarsity.classmanager.common.network.ResponseErrorInterceptor
import com.niloy.daggerhiltdemo.BuildConfig
import com.niloy.daggerhiltdemo.common.network.BlogAPI
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object RetrofitModule {

    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder().build()

    @ErrorInterceptor
    @Provides
    fun provideErrorInterceptor(): Interceptor = ResponseErrorInterceptor()

    @LoggingInterceptor
    @Provides
    fun provideLoggingInterceptor(): Interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Provides
    fun provideHttpClient(
        @LoggingInterceptor loggingInterceptor: Interceptor, @ErrorInterceptor errorInterceptor: Interceptor
    ): OkHttpClient = OkHttpClient.Builder().apply {
        connectTimeout(15, TimeUnit.SECONDS)
        readTimeout(15, TimeUnit.SECONDS)
        writeTimeout(15, TimeUnit.SECONDS)
        // add the error interceptor
        addInterceptor(errorInterceptor)
        /*
        * if app is in debug version
        * then add logging interceptor
        * */
        if (BuildConfig.DEBUG) {
            addInterceptor(loggingInterceptor)
        }
    }.build()

    @Provides
    fun provideRetrofitBuilder(moshi: Moshi, client: OkHttpClient): Retrofit.Builder = Retrofit.Builder()
        .baseUrl("https://open-api.xyz/placeholder/")
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create(moshi))

    @Provides
    fun provideBlogService(retrofit: Retrofit.Builder): BlogAPI = retrofit.build().create(BlogAPI::class.java)

}


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class LoggingInterceptor

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ErrorInterceptor
