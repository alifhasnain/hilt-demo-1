package com.niloy.daggerhiltdemo.common.di

import android.content.Context
import androidx.room.Room
import com.niloy.daggerhiltdemo.common.database.BlogDB
import com.niloy.daggerhiltdemo.common.database.BlogDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object RoomModule {

    @Singleton
    @Provides
    fun provideDB(@ApplicationContext context: Context): BlogDB = Room.databaseBuilder(
        context,
        BlogDB::class.java,
        BlogDB.DB_NAME
    ).fallbackToDestructiveMigration().build()

    @Provides
    fun provideDao(db: BlogDB): BlogDao = db.blogDAO()

}
