package com.niloy.daggerhiltdemo.common.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(tableName = "blogs")
data class Blog(
    @PrimaryKey
    @field:Json(name = "pk") val id: String,
    val title: String,
    val body: String,
    @field:Json(name = "image") val imageUrl: String,
    val category: String
)