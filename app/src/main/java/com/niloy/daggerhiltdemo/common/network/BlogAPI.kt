package com.niloy.daggerhiltdemo.common.network

import com.niloy.daggerhiltdemo.common.models.Blog
import retrofit2.http.GET

interface BlogAPI {

    @GET("blogs")
    suspend fun fetchBlogs(): List<Blog>

}