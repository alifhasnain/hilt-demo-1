package bd.edu.daffodilvarsity.classmanager.common.network

import bd.edu.daffodilvarsity.classmanager.utils.*
import okhttp3.Interceptor
import okhttp3.Response
import org.json.JSONObject

class ResponseErrorInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        if (response.isSuccessful) {
            return response
        } else {
            when(response.code) {
                401 -> {
                    val errorBody = JSONObject(response.body?.string())
                    if(errorBody.getString("message") == "Token expired") {
                        throw TokenExpirationException()
                    } else {
                        throw UnauthorizationException("Error occurred while loading.Please try again")
                    }
                }
                500 -> throw ServerErrorException()
                504 -> throw TimeOutException()
                403 -> throw ForbiddenException()
                469 -> {
                    val errorBody = JSONObject(response.body?.string())
                    throw CustomException(
                        errorBody.getString("message")
                    )
                }
                else -> throw UnknownException()
            }
        }
    }

}