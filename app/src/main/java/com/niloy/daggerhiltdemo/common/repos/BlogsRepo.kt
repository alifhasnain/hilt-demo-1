package com.niloy.daggerhiltdemo.common.repos

import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.niloy.daggerhiltdemo.common.database.BlogDao
import com.niloy.daggerhiltdemo.common.network.BlogAPI
import kotlinx.coroutines.delay

class BlogsRepo(
    private val blogDao: BlogDao,
    private val blogService: BlogAPI
) {

    fun getBlogsLiveData() = blogDao.getCachedBlogsLiveData()
        .switchMap {
            liveData {
                delay(1000)
                emit(it)
            }
        }

    suspend fun fetchAndCacheBlogs() {
        val blogs = blogService.fetchBlogs()
        blogDao.cacheBlogs(blogs)
    }

}