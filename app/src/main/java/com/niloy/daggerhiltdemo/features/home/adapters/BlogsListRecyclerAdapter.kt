package com.niloy.daggerhiltdemo.features.home.adapters

import com.niloy.daggerhiltdemo.R
import com.niloy.daggerhiltdemo.common.adapter.SingleItemBaseAdapter
import com.niloy.daggerhiltdemo.common.models.Blog

class BlogsListRecyclerAdapter: SingleItemBaseAdapter<Blog>() {

    private val blogs: MutableList<Blog> = mutableListOf()

    fun updateItems(updatedList: List<Blog>) {
        blogs.clear();
        blogs.addAll(updatedList)
        notifyDataSetChanged()
    }

    override fun clickedItem(item: Blog) { }

    override fun getItemForPosition(position: Int): Blog = blogs[position]

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.list_item_blogs

    override fun getItemCount(): Int = blogs.size

}