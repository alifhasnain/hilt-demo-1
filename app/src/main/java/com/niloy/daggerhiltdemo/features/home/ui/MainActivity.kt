package com.niloy.daggerhiltdemo.features.home.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import com.niloy.daggerhiltdemo.R
import com.niloy.daggerhiltdemo.common.Utils.makeToast
import com.niloy.daggerhiltdemo.databinding.ActivityMainBinding
import com.niloy.daggerhiltdemo.features.home.adapters.BlogsListRecyclerAdapter
import com.niloy.daggerhiltdemo.features.home.viewmodel.MainActivityViewModel
import com.wada811.databinding.dataBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val binding by dataBinding<ActivityMainBinding>()

    private val viewModel: MainActivityViewModel by viewModels()

    private val adapter by lazy { BlogsListRecyclerAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
        initializeProperties()
        initializeRecyclerView()
        initializeObservers()
        viewModel.fetchBlogs()
    }

    private fun initializeProperties() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            lifecycleScope.launch {
                viewModel.fetchBlogs()
                delay(1000)
                binding.swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.adapter = adapter
    }

    private fun initializeObservers() {
        viewModel.blogsLiveData.observe(this) { blogs ->
            adapter.updateItems(blogs)
        }
        viewModel.toast.observe(this) { makeToast(it.content) }
    }

}



















