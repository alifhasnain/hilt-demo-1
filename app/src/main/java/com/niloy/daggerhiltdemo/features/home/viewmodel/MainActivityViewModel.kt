package com.niloy.daggerhiltdemo.features.home.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.niloy.daggerhiltdemo.common.Utils.Event
import com.niloy.daggerhiltdemo.common.Utils.networkErrorMessage
import com.niloy.daggerhiltdemo.common.models.LoadStates
import com.niloy.daggerhiltdemo.common.repos.BlogsRepo
import kotlinx.coroutines.launch
import timber.log.Timber

class MainActivityViewModel @ViewModelInject constructor(
    private val repo: BlogsRepo,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _uiState: MutableLiveData<LoadStates> = MutableLiveData(LoadStates.Empty())
    val uiState: LiveData<LoadStates> = _uiState

    private val _toast: MutableLiveData<Event<String>> = MutableLiveData()
    val toast: LiveData<Event<String>> = _toast

    val blogsLiveData = repo.getBlogsLiveData()

    fun fetchBlogs() = safeScope {
        repo.fetchAndCacheBlogs()
        _uiState.value = LoadStates.decideEmptyOrNot(blogsLiveData.value, "Nothing found")
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            _uiState.value = LoadStates.Loading
            block()
        } catch (e: Exception) {
            _toast.value = Event(e.networkErrorMessage)
            _uiState.value = LoadStates.Error("Error occurred")
        }
    }

}